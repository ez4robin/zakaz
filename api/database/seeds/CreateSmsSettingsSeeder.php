<?php

use Illuminate\Database\Seeder;

class CreateSmsSettingsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
//        DB::table('sms_settings')->truncate();
        $event_id = 0;
        $sms_statuses = [
          'Новый',
          'В обработке',
          'Принят',
          'На исполнении',
          'Выполнен',
          'Отменен',
          'Отменен с оплатой',
          'Ожидает подтверждения водителем',
          'Отменен водителем',
        ];
        foreach ($sms_statuses as $sms_status) {
        	$smsSetting = \App\Http\Models\SmsSettings::where('event', $sms_status)->first();
        	if (!$smsSetting) {
		        $smsSetting = (new \App\Http\Models\SmsSettings())->fill([
			        'event' => $sms_status,
			        'template' => \App\Http\Models\SmsSettings::getExampleSmsMessage(),
			        'event_id' => $event_id
		        ]);
	        }
          $smsSetting->save();
          $event_id++;
        }
    }
}
