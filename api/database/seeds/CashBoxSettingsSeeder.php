<?php

use App\Http\Models\Site\CashBoxSettings;
use Illuminate\Database\Seeder;

class CashBoxSettingsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
//        DB::table('sms_settings')->truncate();
        $event_id = 0;
        $cash_box_statuses = [
            'Трансфер',
            'Аренда',
            'Доставка',
        ];
        foreach ($cash_box_statuses as $cash_box_status) {
            $smsSetting = CashBoxSettings::where('event_id', $event_id)->first();
            if (!$smsSetting) {
                $smsSetting = (new CashBoxSettings)->fill([
                    'event' => $cash_box_status,
                    'template' => CashBoxSettings::getExampleNotifyMessage(),
                    'event_id' => $event_id
                ]);
            }
            $smsSetting->setAttribute('event', $cash_box_status);
            $smsSetting->save();
            \DB
                ::table('cash_box_settings')
                ->whereNotIn('id', [$smsSetting->getAttribute('id')])
                ->where('event_id', $event_id)
                ->delete();
            $event_id++;
        }
    }
}
