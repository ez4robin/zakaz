<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateClientsTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('clients', function (Blueprint $table) {
      $table->increments('id');
      $table->string('first_name');
      $table->string('last_name');
      $table->string('phone');
      $table->string('password');
      $table->string('token');
      $table->boolean('blocked')->default(0);
      $table->timestamps();
    });

    Schema::create('uid', function (Blueprint $table) {
      $table->increments('id');
      $table->integer('client_id')->unsigned()->nullable();
      $table->foreign('client_id')->references('id')->on('clients')->onDelete('cascade');
      $table->string('uid', 1024)->nullable();
      $table->timestampsTz();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('uid');
    Schema::dropIfExists('clients');
  }
}
