<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeDateStartEndToTimestamp extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('schedule', function (Blueprint $table) {
           $table->dropColumn('date_start');
           $table->dropColumn('date_end');
        });
        Schema::table('schedule', function (Blueprint $table) {
           $table->timestamp('date_start');
           $table->timestamp('date_end');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('schedule', function (Blueprint $table) {
            $table->dropColumn('date_start');
            $table->dropColumn('date_end');
        });
        Schema::table('schedule', function (Blueprint $table) {
            $table->datetime('date_start');
            $table->datetime('date_end');
        });
    }
}
