<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DefaultNullProfile extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
    {
        Schema::table('profile', function (Blueprint $table) {
            $table->string('sms_code')->default('')->change();
            $table->text('token')->nullable()->change();
            $table->string('callsign')->default('')->change();
            $table->string('name')->default('')->change();
            $table->text('requisites')->nullable()->change();
            $table->string('phone')->default('')->change();
            $table->string('address')->default('')->change();
            $table->string('legal_address')->default('')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
