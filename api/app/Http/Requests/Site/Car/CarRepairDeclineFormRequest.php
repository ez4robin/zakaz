<?php


namespace App\Http\Requests\Site\Car;

use App\Http\Models\Site\Car\CarRepair;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Contracts\Validation\Validator;

class CarRepairDeclineFormRequest extends FormRequest
{
	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}
	
	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			['id', 'exists:cars,id', function($attribute, $carId, $fail) {
				if (!$this->checkAvailabilityToDecline($carId)) {
					$fail('Нельзя изменить статус заявки');
				}
			},]
		];
	}
	
	protected function checkAvailabilityToDecline($carId) {
		$carStatusChangeRequest = CarRepair::where([
			['car_id', '=', $carId],
			['status', '=', CarRepair::STATUS_REQUESTED]
		])->first();
		return $carStatusChangeRequest;
	}
	
	protected function failedValidation(Validator $validator)
	{
		throw new HttpResponseException(response()->json([
			'status' => 'failure',
			'messages' => $validator->errors()->all()
		], 403));
	}
}
