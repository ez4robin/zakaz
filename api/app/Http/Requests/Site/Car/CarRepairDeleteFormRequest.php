<?php


namespace App\Http\Requests\Site\Car;

use App\Http\Models\Site\Car\CarRepair;
use App\Http\Models\UserCars;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Contracts\Validation\Validator;

class CarRepairDeleteFormRequest extends FormRequest
{
	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}
	
	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			['id', 'exists:cars,id', function($attribute, $carId, $fail) {
				if (!$this->determineAvailabilityToCreateStatusChangeRequestByCarId($carId)) {
					$fail('Автомобиль отсутствует у водителя');
				}
			},]
		];
	}
	
	protected function determineAvailabilityToCreateStatusChangeRequestByCarId($carId) {
		$userId = \Auth::user()->getAuthIdentifier();
		$userCar = UserCars::where([
			['user_id', '=', $userId],
			['car_id', '=', $carId]
		])->first();
		return $userCar;
	}
	
	protected function failedValidation(Validator $validator)
	{
		throw new HttpResponseException(response()->json([
			'status' => 'failure',
			'messages' => $validator->errors()->all()
		], 403));
	}
}
