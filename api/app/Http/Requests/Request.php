<?php


namespace App\Http\Requests;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\ValidationException;

abstract class Request extends FormRequest
{
	/**
	 * Force response json type when validation fails
	 * @var bool
	 */
	protected $forceJsonResponse = true;
	
	/**
	 * Get the proper failed validation response for the request.
	 *
	 * @param  array  $errors
	 * @return \Symfony\Component\HttpFoundation\Response
	 */
//	public function response(array $errors)
//	{
//		if ($this->forceJsonResponse || $this->ajax() || $this->wantsJson()) {
//		abort(403, $errors);
//			return new JsonResponse($errors, 403);
//		}
		
//		return $this->redirector->to($this->getRedirectUrl())
//			->withInput($this->except($this->dontFlash))
//			->withErrors($errors, $this->errorBag);
//	}
	
	protected function failedValidation(Validator $validator)
	{
		$errors = (new ValidationException($validator))->errors();
		throw new HttpResponseException(
			response()->json(['errors' => $errors], JsonResponse::HTTP_UNPROCESSABLE_ENTITY)
		);
	}
}
