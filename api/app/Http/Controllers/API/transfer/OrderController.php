<?php

namespace App\Http\Controllers\API\transfer;

use App\Http\Controllers\Controller;
use App\Http\Models\transfer\Order;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class OrderController extends Controller
{
    public function index(Request $request)
    {
        return Order::searchClient($request);
    }

    public function createRent(Request $request)
    {
        return (new Order())->storeRent($request);
    }

    public function status(Request $request)
    {
        if ($order = Order::where([
            ['id', '=', $request->get('id')],
            ['client_id', '=', Auth::user()->getAttribute('id')]
        ])->first()) {
            $order->setAttribute('status', $request->get('status'));
            if ($result = $order->save()) {
                return response()->json([
                    "status" => "success",
                ], 200);
            }
        }
        else {
            $result = "Не найден заказ с таким номером";
        }
        return response()->json([
            "status" => "failure",
            "errors" => [$result]
        ], 403);
    }

    public function create(Request $request)
    {
        return (new Order())->store($request);
    }

    public function get(Request $request)
    {
        $model = Order::find($request->get('id'));
        if (!$model) {
            return response()->json([
                'status' => 'failure',
                'messages' => [
                    'Заказ не найден'
                ]
            ]);
        } else {
            $model->driver->profile;
            $model->car;
            $model->addresses = DB::table('order_address')
                ->select('where')
                ->where('order_id', '=', $request->get('id'))
                ->get();
            return response()->json([
                'model' => $model,
                'options' => [
                    'status' => Order::getStatusArray()
                ],
                'status' => 'success'
            ]);
        }
    }

    public function decline(Request $request)
    {
        $model = Order::find($request->get('id'));
        if (!$model) {
            return response()->json([
                'status' => 'failure',
                'messages' => [
                    'Заказ не найден'
                ]
            ]);
        } else if ($model->decline($request)){
            return response()->json([
                'status' => 'success',
                'model' => $model
            ], 200);
        }
    }
    public function delete($id)
    {
        return response()->json(Order::find($id)->delete());
    }
}
