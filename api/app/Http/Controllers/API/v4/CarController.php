<?php

namespace App\Http\Controllers\API\v4;

use App\Http\Controllers\Controller;
use App\Http\Models\Api\v4\Car;
use App\Http\Requests\v4\UpdateCarTariff;
use Illuminate\Http\Request;

class CarController extends Controller
{
	public function driverCars(Request $request)
	{
		return response()->json(Car::driverCars($request), 200);
	}


    public function getCarsForOrder(Request $request)
    {
        return Car::searchOrdering($request);
    }

    public function getCarsForOrderTransfer(Request $request)
    {
        return Car::searchOrderingTransfer($request);
    }

	public function updateTariff(UpdateCarTariff $request)
	{
		$car = Car::find($request->get('id'));
		return $car->storeTariff($request);
	}
}
