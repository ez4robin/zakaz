<?php


namespace App\Http\Models\Site\Report;


use App\Http\Models\Site\CashBoxSettings;
use Illuminate\Database\Eloquent\Model;

class CheckTemplate extends Model {
    protected $templateMessage;
    protected $filteredMessage;
    protected $fillable = ['order_id'];
    protected $options;
    public $order;
    public $cashBox;
    public function __construct($order)
    {
        $this->order = $order;
        $this->initTemplateMessage();
        $this->setCanSendAttribute();
        $this->setAttribute('order_id', $order->getAttribute('id'));
        $this->filterMessage();
        parent::__construct();
    }

    public function getTemplates()
    {
        return [
            [
                'template' => 'ЦВЕТ',
                'value' => $this->getColor()
            ],
            [
                'template' => 'ТС',
                'value' => $this->getCar()
            ],
            [
                'template' => 'ГОСНОМЕР',
                'value' => $this->getCarNumber()
            ],
            [
                'template' => 'ВОДИТЕЛЬ',
                'value' => $this->getDriver()
            ],
            [
                'template' => 'ТЕЛВОДИТЕЛЬ',
                'value' => $this->getDriverNumber()
            ],
            [
                'template' => 'СУММА',
                'value' => $this->getPrice()
            ],
            [
                'template' => 'НОМЕРЗАКАЗА',
                'value' => $this->getOrderId()
            ],
            [
                'template' => 'ВРЕМЯПОДАЧИ',
                'value' => $this->getTime()
            ],
            [
                'template' => 'ДАТАПОДАЧИ',
                'value' => $this->getDate()
            ],
            [
                'template' => 'АДРЕСПОДАЧИ',
                'value' => $this->getAddressFrom()
            ],
            [
                'template' => 'АДРЕСКУДА',
                'value' => $this->order->getAddressesArray()
            ],
            [
                'template' => 'ПРИМЕЧАНИЕ',
                'value' => $this->getComment()
            ]
        ];
    }

    public function getCanSend() {
        return $this->cashBox->send_notify;
    }

    public function initTemplateMessage() {
        $order_type = $this->order->getAttribute('order_type');
        $this->cashBox = CashBoxSettings::where('event_id', '=', $order_type)->first();
        $this->templateMessage = $this->cashBox->getAttribute('template');
    }

    public function setCanSendAttribute() {
        if ($this->cashBox->getAttribute('send_notify')) {
            return true;
        } else {
            return false;
        }
    }

    public function getComment() {
        if (isset( $this->order->comment)) {
            return $this->order->comment;
        } else {
            return "";
        }
    }

    public function getAddressFrom() {
        if (isset($this->order->from)) {
            return $this->order->from;
        } else {
            return "";
        }
    }

    public function getDate() {
        if (isset($this->order->datetime_order)) {
            return \Carbon\Carbon::parse($this->order->datetime_order)->format("d.m.Y");
        } else {
            return "";
        }
    }

    public function getTime() {
        if (isset($this->order->datetime_order)) {
            return \Carbon\Carbon::parse($this->order->datetime_order)->format("H:i");
        } else {
            return "";
        }
    }

    public function getOrderId() {
        if (isset($this->order->id)) {
            return $this->order->id;
        } else {
            return "";
        }
    }

    public function getPrice() {
        if (isset($this->order->price)) {
            return $this->order->price;
        } else {
            return "";
        }
    }

    public function getDriverNumber() {
        if (isset($this->order->driver->profile->phone)) {
            return $this->order->driver->profile->phone;
        } else {
            return "";
        }

    }

    public function getDriver() {
        if (isset($this->order->driver->profile->name)) {
            return $this->order->driver->profile->name;
        } else {
            return "";
        }
    }

    public function getCarNumber() {
        if (isset($this->order->car->car_number)) {
            return $this->order->car->car_number;
        } else {
            return "";
        }
    }

    public function getCar() {
        if (isset($this->order->car->mark)) {
            return $this->order->car->mark . ' ' . $this->order->car->model;
        } else {
            return "";
        }
    }

    public function getColor() {
        if (isset($this->order->car->color)) {
            return $this->order->car->color;
        } else {
            return "";
        }
    }

    public function filterMessage() {
        foreach ($this->getTemplates() as $template) {
            $this->setFilteredMessage($this->replaceMessage($this->getTemplateByWord($template['template']), $template['value'], $this->getTemplate()));
            $this->setTemplate($this->getFilteredMessage());
        }
    }

    public function getTemplateByWord($template) {
        return '<'.$template.'>';
    }


    public function getClientNumber() {
        if (isset($this->order->number)) {
            return $this->order->number;
        } else {
            return "";
        }
    }

    public function getTemplate() {
        return $this->templateMessage;
    }

    protected function replaceMessage($template, $replace, $text) {
        $replacedText = str_replace($template, $replace, $text);
        return $replacedText;
    }

    public function setTemplate($templateMessaage): void {
        $this->templateMessage = $templateMessaage;
    }

    protected function setFilteredMessage($filteredMessage): void {
        $this->filteredMessage = $filteredMessage;
    }

    public function getFilteredMessage() {
        return $this->filteredMessage;
    }
}
