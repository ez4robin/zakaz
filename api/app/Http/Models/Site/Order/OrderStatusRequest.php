<?php
namespace App\Http\Requests\v3;

use App\Http\Models\Site\Report\Report;
use App\Http\Models\User\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use App\Http\Models\Order;
use App\Http\Models\UserCars;

class OrderStatusRequest extends Order
{
    protected $table = 'orders';

    public $fillable = ['driver_id', 'comment', 'driver_payment', 'car_id', 'status'];
    public static function boot()
    {

        self::created(function(Order $model){
            if ($token = $model->isSetDriverToken($model))
                $model->pushMessageCreated($model, $token);
            $driver_managers = User::role('driver-manager')->get();
            foreach($driver_managers as $driver_manager)
            {
                $token = $driver_manager->profile->token;
                $model->pushMessageCreated($model, $token);
            }
	        self::sendSmsByStatus($model);
        });
        self::updating(function(OrderStatusRequest $model){

        });

        self::updated(function(OrderStatusRequest $model){
            if ($model->status == 5 && Auth::user()->hasRole('driver-manager') && $model->getOriginal('status') != 5)
            {
                if ($token = $model->isSetDriverToken($model))
                    $model->pushMessageCancelled($model, $token);
            }
            if ($model->status == 3 && Auth::user()->hasRole('driver-manager') && $model->getOriginal('status') != 3)
            {
                if ($token = $model->isSetDriverToken($model))
                    $model->pushMessageCreated($model, $token);
            }
            if ($model->status == 5 && Auth::user()->hasRole('driver') && $model->getOriginal('status') != 5)
            {
                $driver_managers = User::role('driver-manager')->get();
                foreach($driver_managers as $driver_manager)
                {
                    $token = $driver_manager->profile->token;
                    $model->pushMessageCancelled($model, $token);
                }
            }
	
	        $model->sendCheck($model);
	        self::sendSmsByStatus($model);
        });

        self::deleting(function($model){
            // ... code here
        });

        self::deleted(function($model){
            // ... code here
        });
    }
    
    
    public function changeStatus(Request $request)
    {
        $user = Auth::user();
        if ($user->hasRole('admin') || $user->hasRole('driver-manager'))
        {
            $validator = Validator::make(Input::all(), $this->validateAdmin($user, $request), $this->messages());
            if ($validator->fails()) {
                return response()->json([
                'status' => 'failure',
                'errors' => $validator->errors()->all()
            ], 403);
            }
            else
            {
                $this->fill($request->all());
                if ($result = $this->save())
                {
                    return response()->json($result, 200);
                }
                else
                {
                    return response()->json($result, 403);   
                }
            }
        }
        elseif($user->hasRole('driver'))
        {
            
            $validator = Validator::make(Input::all(), $this->validateDriver($user, $request), $this->messages());
            if ($validator->fails()) {
                return response()->json([
                    'status' => 'failure',
                    'errors' => $validator->errors()->all()
                ], 403);
            }
            else
            {
                $this->status = $request->get('status');
                $this->driver_comment = $request->get('driver_comment');
                return response()->json($this->save());
            }
        }

    }

    public function setDriversCars($user)
    {
        if (!$this->driver_id)
        {
            if ($car = UserCars::leftJoin('cars', 'user_cars.car_id', '=', 'cars.id')
                    ->where([
                        ['user_cars.user_id', '=', $user->id],
                        ['user_cars.blocked', '=', 0],
                    ])
                    ->first())
            {
                $this->driver_id = $user->id;
            }
        }
        if (!$this->car_id)
        {
            if ($this->driver_id)
            {
                if ($car = UserCars::leftJoin('cars', 'user_cars.car_id', '=', 'cars.id')
                    ->where([
                        ['user_cars.user_id', '=', $this->driver_id],
                        ['user_cars.blocked', '=', 0],
                    ])
                    ->first())
                {
                    $this->car_id = $car->car_id;
                }
                else
                {
                    $this->driver_id = null;
                }
            }
        }
    }

    public function validateDriver($user, $request)
    {
        return [
                'id' => ['required', function($attribute, $value, $fail) use($user){
                    if (!($user->id == $this->driver_id))
                    {
                        $fail("Заказ не принадлежит водителю");
                    }
                }],
                'driver_comment' => 'sometimes',
                'status' => ['required', 'numeric', 'min:1', 'max:8', function($attribute, $value, $fail){
                    if ($this->status == $value )
                    {
                        return true;
                    }
                    if ($this->status == 2)
                    {
                        if (!($value == 3 || $value == 5))
                        {
                            $fail("Невозможно изменить статус");
                        }
                    }
                    elseif ($this->status == 3)
                    {
                        if (!($value == 4 || $value == 5))
                        {
                             $fail("Невозможно изменить статус");
                        }
                    }
                    elseif ($this->status == 5)
                    {
                        if (!($value == 5))
                             $fail("Невозможно изменить статус");
                    }
	                if ($this->status == 8) {
		                $fail("Невозможно изменить статус отмененного заказа");
	                }
                    else {
                         $fail("Невозможно изменить статус");
                    }
                }],
            ];
    }
    public function validateAdmin($user, $request)
    {
        return [
                'driver_id' => ['sometimes', function($attribute, $value, $fail){
                    if (!($user = User::where('id', '=', $value)->first())) {
                         $fail('Водитель не найден');
                    }
                    else
                    {
                        if (!($user->hasRole('driver') || $user->hasRole('driver-manager')))
                             $fail('Водитель не найден');
                    }
                }
                ],
                'comment' => ['sometimes'],
                'driver_payment' => ['nullable', 'numeric'],
                'car_id' => ['sometimes', 'exists:cars,id', function($attribute, $value, $fail) use ($request){
                    if ($request->driver_id && $request->car_id)
                    {
                        if (!DB::table('user_cars')->where([
                            ['user_id', '=', $request->driver_id],
                            ['car_id', '=', $request->car_id]
                        ])->first())
                        {
                             $fail('Авто не принадлежит водителю');
                        }
                    }
                }],
                'status' => ['required', 'numeric', 'min:1', 'max:7'],
            ];
    }

    public function messages() {
        return [
            'required' => 'Заполните это поле',
            'status.required' => 'Заполните статус',
            'status.min' => 'Неправильный статус',
            'status.max' => 'Неправильный статус',
            'id.required' => 'Заполните номер заказа',
            'min' => 'Не менее :min символа(-ов)',
            'max' => 'Не более :max символа(-ов)',
            'unique' => 'Уже используется',
            'email' => 'Введите правильный формат email',
            'date' => 'Выберите дату',
            'numeric' => 'Заполните это поле',
            'driver_payment.numeric' => 'Оплата водителю должно быть числом',
            'car_id.exists' => 'Нет такого автомобиля'
        ];
    }
}
