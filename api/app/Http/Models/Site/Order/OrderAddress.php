<?php


namespace App\Http\Models;


use Illuminate\Database\Eloquent\Model;

class OrderAddress extends Model {
    public $timestamps = false;
    protected $table = 'order_address';
    protected $fillable = ['order_id', 'where'];
}
