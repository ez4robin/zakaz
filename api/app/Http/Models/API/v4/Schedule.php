<?php

namespace App\Http\Models\Api\v4;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;

class Schedule extends \Eloquent
{
	public $timestamps = false;
    protected $table = 'schedule';

    public static function search(Request $request)
	{
		$rows = self::query()
            ->where('user_id', '=', Auth::user()->getAttribute('id'))
			->where([
				['date_start', '>', Carbon::parse($request->get('date_start'))->hour(0)->second(0)->minute(0)],
				['date_end', '<', Carbon::parse($request->get('date_end'))->hour(23)->second(59)->minute(59)],
			])
            ->when($request->get('orderBy'), function ($rows) use ($request) {
				return $rows
					->orderBy($request->get('orderBy'), $request->get('desc') == 'true' ? 'desc' : 'asc');
			})
			->when(!$request->get('orderBy'), function ($rows) use ($request) {
				return $rows
					->orderBy('schedule.id', 'desc');
			})
			->groupBy('id')
            ->get();
		return response()->json([
			'models' => $rows
		]);
	}

    public function rules(string $requestMethod)
    {
    	return [
    		'date_start' => 'required|date',
    		'date_end' => 'required|date',
    	];
    }
	public function messages() {
		return [
			'required' => 'Заполните это поле',
			'min' => 'Не менее :min символа(-ов)',
			'max' => 'Не более :max символа(-ов)',
			'car_id.min' => 'Заполните это поле',
			'date' => 'Введите время',
			'unique' => 'Уже используется',
			'email' => 'Введите правильный формат email',
		];
	}


    protected $fillable = [
        'date_start', 'date_end'
    ];
//
//    protected $casts = [
//	    'date_start' => 'datetime',
//	    'date_end' => 'datetime'
//    ];

    public function store(Request $request)
    {
		$validator = Validator::make(Input::all(), $this->rules($request), $this->messages());
		if ($validator->fails()) {
			return response()->json($validator->messages(), 403);
		}
		else
		{
			$this->fill($request->all());
			$this->setUserIdCurrent();
			if ($result = $this->save())
			{
				return response()->json($this, 200);
			}
			else
				return response()->json($result, 403);
		}
    }
    public static function batchInsert($arrayOfAttributesOfModels) {
    	$countSaved = 0;
	    foreach($arrayOfAttributesOfModels as $attributesOfModel)
	    {
		    if($attributesOfModel){
			    $model = new self(); // this is to have new instance of own
			    $model->setUserIdCurrent();
			    $model->fill($attributesOfModel);
			    try {
				    $model->save();
				    $countSaved++;
			    } catch (\Exception $exception) {
//				    \Log::critical('schedule insert error', $exception);
				    if (is_array($arrayOfAttributesOfModels)) {
				    	return response()->json([
						    'message' => 'Не удалось сохранить '.(count($arrayOfAttributesOfModels) - $countSaved).' из '.count($arrayOfAttributesOfModels),
						    'status' => 'Failure'
					    ], 500);
				    } else {
					    return response()->json([
						    'message' => 'Серверная ошибка',
						    'status' => 'Failure'
					    ], 500);
				    }
			    }
		    }
	    }
	    return response()->json([
	    	'message' => 'Успешно сохранено '.$countSaved.' записей',
		    'status' => 'Success'
	    ], 200);
    }
//
//    public function getDateStartAttribute($value) {
//	    return $this->getMilliSecondsTimestamp(Carbon::parse($value)->getTimestamp());
//    }
//
//    public function getDateEndAttribute($value) {
//	    return $this->getMilliSecondsTimestamp(Carbon::parse($value)->getTimestamp());
//    }
    
    public function getMilliSecondsTimestamp($timestamp) {
    	return $timestamp * 1000;
    }
    
    public function setDateStartAttribute($value) {
	    $this->attributes['date_start'] = Carbon::parse($value)->toDateTimeString();
    }
    
    public function setDateEndAttribute($value) {
    	$this->attributes['date_end'] = Carbon::parse($value)->toDateTimeString();
    }
    
    public function setUserIdCurrent() {
	    $this->setAttribute('user_id', Auth::user()->getAttribute('id'));
    }
}
