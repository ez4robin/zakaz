<?php


namespace App\Http\Models\API\v4;


use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
  protected $fillable = [
    'first_name',
    'last_name',
    'phone',
   ];
  protected $hidden = [
      'password',
      'token'
  ];
}
