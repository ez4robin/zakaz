<?php
/**
 * Created by PhpStorm.
 * User: Acer
 * Date: 19.04.2018
 * Time: 0:34
 */
Route::post('auth-phone', 'v4\Auth\LoginController@authenticatePhone');
Route::post('auth-sms', 'v4\Auth\LoginController@authenticateSms');
Route::middleware(['jwt.auth', 'role:driver|admin|legal|driver-manager'])->get('orders/', 'v4\OrderController@index');
Route::middleware(['jwt.auth', 'role:admin|legal|driver-manager'])->get('drivers/', 'v4\UserController@getDrivers');
Route::middleware(['jwt.auth', 'role:admin|driver|driver-manager'])->post('order-status', 'v4\OrderController@status');
Route::middleware(['jwt.auth', 'role:admin|legal|driver|driver-manager'])->get('profile/{id}', 'v4\ProfileController@get');
