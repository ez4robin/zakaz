export class Car {
    id: number;
    mark: string;
    model: string;
    car_number: string;
    color: string;
    year: number;
    passengers: number;
    baggage: number;
    baby_chair: any;
    conditioner: any;
    bank_card_payment: any;
    repairing: any;
    comment: string;
    address: string;
    request_price: number;
    rent_price: number;
    transfer: number;
    order_cost: number;
    image: string;
    file: string;
    commission: number;

    getImagePath() {
        return 'api/public/images/cars/';
    }
}
