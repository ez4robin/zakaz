export class DriverBalance{
    value: number;
    comment: string;
    driver_id: string;
    type : string;
    constructor(object: Partial<DriverBalance> = undefined) {
        object ? Object.assign(this, object) : undefined;
    }
    getIsIncrement() {
        if (this.type == 'increment_by_user' || this.type ==  'increment_by_status') {
            return true;
        } else {
            return false;
        }
    }
}
