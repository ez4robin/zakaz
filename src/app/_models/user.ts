﻿export class User {
    id: number;
    name: string;
    password: string;
    email: string;
    role: string;
    blocked: number;
    roles : any = [];
    rolesUpdate : any = {};
    hasRole(roleName)
    {
        return this.roles.find(x => {
            return x.name === roleName;
        });
    }
    getRoles()
    {
        return [
            { value: 'admin', name: 'Администратор' },
            { value: 'driver-manager', name: 'Менеджер' },
            { value: 'driver', name: 'Водитель' },
            { value: 'legal', name: 'Юр.лицо' },
            { value: 'client', name: 'Клиент' },
        ];
    }
    getRolesWithIds()
    {
        return [
            { id: 'admin', name: 'Администратор' },
            { id: 'driver-manager', name: 'Менеджер' },
            { id: 'driver', name: 'Водитель' },
            { id: 'legal', name: 'Юр.лицо' },
            { id: 'client', name: 'Клиент' },
        ];
    }
}
