import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SmsSettingsListComponent } from './sms-settings-list/sms-settings-list.component';
import {RouterModule} from '@angular/router';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

const Routes = [
  {
    'path' : '',
    'component' : SmsSettingsListComponent
  }
];

@NgModule({
  declarations: [SmsSettingsListComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(Routes)
  ]
})
export class SmsSettingsModule { }
