import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {Grid} from './grid/grid';
import {FormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {RouterModule} from '@angular/router';


@NgModule({
    exports: [
        Grid
    ],
    declarations: [
        Grid
    ],
    imports: [
        CommonModule,
        FormsModule,
        RouterModule,
        HttpClientModule,
    ]
})
export class PartialsModule {
}
