import {
    Component,
    Input,
    Output,
    EventEmitter,
    TemplateRef,
    ElementRef,
    ViewChildren,
    ViewContainerRef
} from '@angular/core';
import {OnChanges, SimpleChange} from '@angular/core';
import {Column} from './column';
import {Sorter} from './sorter';
import * as _ from 'underscore';

@Component({
    selector: 'grid',
    templateUrl: './grid.html'
})
export class Grid {
    sorter: Sorter;
    pager: any;
    search = {};
    _sort: string = 'id';
    desc: boolean = true;
    @Input() columns: Array<Column>;
    @Input() rows: Array<any>;
    @Input() total: number;
    @Input() action: string;
    @Input() currentPage: number;
    @Input() pageSize: number;
    @Input() events: string = 'edit|delete';
    @Input() test: any;
    @Output() update = new EventEmitter<number>();
    @Output() delete = new EventEmitter<number>();
    @Output() block = new EventEmitter<number>();
    @Output() setSearch = new EventEmitter<any>();
    @Output() setsort = new EventEmitter<any>();

    constructor() {
        this.sorter = new Sorter();
    }

    ngOnChanges(changes: { [propKey: string]: SimpleChange }) {
        let changedProp = changes['total'];
        if (changedProp) {
            this.pager = this.getPager(changedProp.currentValue, 1);
        }
    }

    ngOnInit() {
        this.pager = this.getPager(this.total, this.currentPage);
    }

    inputSearch() {
        if (this.setSearch.observers.length > 0) {
            this.setSearch.emit(this.search);
        }
    }

    sort(key) {
        if (this._sort == key) this.desc = !this.desc;
        this._sort = key;
        this.setsort.emit({
            key: key,
            desc: this.desc
        });
    }

    blockItem(id: number) {
        this.block.emit(id);
    }

    deleteItem(id: number) {
        if (confirm('Точно удалить'))
            this.delete.emit(id);
    }

    setPage(page: number) {
        if (page > this.getMaxPage(this.total) || page < 1)
            return false;

        this.pager = this.getPager(this.total, page);
        this.update.emit(page);
        // this.getPager(this.total, this.currentPage);
    }

    maxDate() {
        var d = new Date();
        var year = d.getFullYear();
        var month = d.getMonth();
        var day = d.getDate();
        return new Date(year + 1, month, day);
    }

    hasSearch() {
        return this.columns.filter(e => e['search']).length > 0;
    }

    getMaxPage(totalItems: number, pageSize: number = 10) {
        let totalPages = Math.ceil(totalItems / pageSize);
        return totalPages;
    }

    getPager(totalItems: number, currentPage: number = 1, pageSize: number = 10) {
        // calculate total pages
        let totalPages = Math.ceil(totalItems / pageSize);

        let startPage: number, endPage: number;
        if (totalPages <= 10) {
            // less than 10 total pages so show all
            startPage = 1;
            endPage = totalPages;
        } else {
            // more than 10 total pages so calculate start and end pages
            if (currentPage <= 6) {
                startPage = 1;
                endPage = 10;
            } else if (currentPage + 4 >= totalPages) {
                startPage = totalPages - 9;
                endPage = totalPages;
            } else {
                startPage = currentPage - 5;
                endPage = currentPage + 4;
            }
        }

        // calculate start and end item indexes
        let startIndex = (currentPage - 1) * pageSize;
        let endIndex = Math.min(startIndex + pageSize - 1, totalItems - 1);

        // create an array of pages to ng-repeat in the pager control
        let pages = _.range(startPage, endPage + 1);

        // return object with all pager properties required by the view
        return {
            totalItems: totalItems,
            currentPage: currentPage,
            pageSize: pageSize,
            totalPages: totalPages,
            startPage: startPage,
            endPage: endPage,
            startIndex: startIndex,
            endIndex: endIndex,
            pages: pages
        };
    }
}
