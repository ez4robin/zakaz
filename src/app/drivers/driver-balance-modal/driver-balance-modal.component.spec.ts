import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DriverBalanceModalComponent } from './driver-balance-modal.component';

describe('DriverBalanceModalComponent', () => {
  let component: DriverBalanceModalComponent;
  let fixture: ComponentFixture<DriverBalanceModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DriverBalanceModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DriverBalanceModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
