import { Injectable } from '@angular/core';

@Injectable()
export class StorageHelper {
   constructor() {

   }
	public getUserName()
	{
		var storage = JSON.parse(localStorage.getItem('currentUser'));
		if (storage && storage.user)
		  return storage.user.name;
		else
		  return false;
	}

	public getUserId()
	{
		var storage = JSON.parse(localStorage.getItem('currentUser'));
		if (storage && storage.user)
		  return storage.user.id;
		else
		  return false;
	}

	public hasRole(roleName)
	{
		var storage = JSON.parse(localStorage.getItem('currentUser'));
		if (storage && storage.roles)
		{
			return storage.roles.indexOf(roleName) >= 0;
		}
		return false;
	}

	public getUserRoles()
	{
		var storage = JSON.parse(localStorage.getItem('currentUser'));
		if (storage && storage.roles)
		  return storage.roles;
		else
		  return false; 
	}	
	getStartUrl(){
		return this.getReturnUrl();
	}
	getLoginUrl(){
		return "login";
	}
	getReturnUrl() {
		return "orders";
	}
}