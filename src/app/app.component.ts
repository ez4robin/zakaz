﻿import {Component, OnInit} from '@angular/core';
import {StorageHelper} from './_helpers/storage.helper';
import {Router} from '@angular/router';

declare var ymaps: any;
declare var google: any;

@Component({
  moduleId: module.id.toString(),
  selector: 'app',
  templateUrl: 'app.component.html'
})

export class AppComponent implements OnInit {
  constructor(
    private storageHelper: StorageHelper,
    private router: Router,
  ) {
    this.router = router;
  }

  ngOnInit() {
    this.router.events.subscribe((url: any) => {
      if (url.url && url.url === '/') {
        console.log(url.url);
        this.router.navigate(['/' + this.storageHelper.getStartUrl()]);
      }
    });
    console.log(this.storageHelper.getStartUrl());
    console.log(this.router.url);
    this.loadYmapsIfUndefined();
  }

  loadYmapsIfUndefined() {
    if (typeof ymaps === 'undefined') {
      console.log('load ymaps');
      let s = document.createElement('script');
      s.src = 'https://api-maps.yandex.ru/2.1/?lang=ru_RU';
      document.body.appendChild(s);
      // the variable is defined
    }
    // if (typeof google == 'undefined') {
    // 	console.log('load google');
    // 	var s = document.createElement('script');
    // 	s.src = 'https://maps.googleapis.com/maps/api/js?key=AIzaSyD9UHfcRReCgT8_MmeqNvoK6QAWvcEeHrM&libraries=places';
    // 	document.body.appendChild(s);
    //     // the variable is defined
    // }
  }

}
