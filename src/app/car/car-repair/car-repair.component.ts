import {Component, OnInit} from '@angular/core';
import {HttpClient} from '@angular/common/http';

import {ColumnMode} from '@swimlane/ngx-datatable';
@Component({
    selector: 'app-car-repair',
    templateUrl: './car-repair.component.html',
    styleUrls: ['./car-repair.component.scss']
})
export class CarRepairComponent implements OnInit {
    title: String = 'Список заявок на ремонт';
    rows: Array<any> = [];
    columns: Array<any> = [
        { prop: 'car_id', name: 'Автомобиль'},
        { prop: 'status', name: 'Статус'},
        { prop: 'changed_by_user_id', name: 'Кем изменен' },
        { prop: 'created_by_user_id', name: 'Кем добавлен' },
    ];
    statusList = [
        {
            id : 0,
            label : 'Отказано'
        }, {
            id : 1,
            label : 'Новая заявка'
        }, {
            id : 2,
            label : 'Принят'
        }];
    count: any = null;
    ColumnMode = ColumnMode;
    constructor(protected http: HttpClient) {
        this.getCarRepairRequests();
    }

    getCarRepairRequests() {
        this.http.get('/api/car-repair').subscribe((data) => {
            this.rows = data['models'];
        });
    }

    ngOnInit() {
    }

    acceptRepair(row) {
        this.http.post('/api/car-repair-accept', {
            id : row['id']
        }).subscribe((data) => {
            this.getCarRepairRequests();
        });
    }

    declineRepair(row) {
        this.http.post('/api/car-repair-decline', {
            id : row['id']
        }).subscribe((data) => {
            this.getCarRepairRequests();
        });
    }

    getStatusById(status) {
        return this.statusList.find((statusItem) => {
            return statusItem.id === status;
        }).label;
    }

    carPipe(car: any) {
        return car.mark + ' ' + car.model + ' ' + car.car_number + ' ' + car.color;
    }

    statusPipe(status: any) {
        if (status) {
            return this.getStatusById(status);
        } else {
            return 'Неизвестный статус';
        }
    }

    changeStatus(status, row) {
        this.http.get('/api/car-repair').subscribe((data) => {
            this.rows = data['models'];
        });
    }

    changedByUserId(user: any) {
        if (user) {
            return user.profile.name;
        }
    }

    createdByUserId(user: any) {
        if (user) {
            return user.profile.name;
        }
    }
}
