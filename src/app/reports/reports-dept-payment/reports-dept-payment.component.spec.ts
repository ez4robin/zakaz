import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportsDeptPaymentComponent } from './reports-dept-payment.component';

describe('ReportsDeptPaymentComponent', () => {
  let component: ReportsDeptPaymentComponent;
  let fixture: ComponentFixture<ReportsDeptPaymentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportsDeptPaymentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportsDeptPaymentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
