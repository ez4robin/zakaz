import { Component, OnInit } from '@angular/core';
import {HttpClient} from '@angular/common/http';

import {ColumnMode} from '@swimlane/ngx-datatable';
@Component({
  selector: 'app-reports-payment',
  templateUrl: './reports-payment.component.html',
  styleUrls: ['./reports-payment.component.css']
})
export class ReportsPaymentComponent implements OnInit {
  form = {
    date_from: null,
    date_to: null
  };
  ColumnMode = ColumnMode;
  rows = [

  ];
  columns = [
    { prop: 'driver_name', name: 'Имя'},
    { prop: 'driver_payment', name: 'Доход' },
  ];
  constructor(
    protected http: HttpClient
  ) {
  }
  ngOnInit() {
  }
  getReport() {
    this.http.post('/api/report', this.form).subscribe((data: any) => {
      this.rows = data;
    }, (data) => {
      console.log(data);
    });
  }
  filter() {
    this.getReport();
  }
}
