import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportsPaymentComponent } from './reports-payment.component';

describe('ReportsPaymentComponent', () => {
  let component: ReportsPaymentComponent;
  let fixture: ComponentFixture<ReportsPaymentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportsPaymentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportsPaymentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
