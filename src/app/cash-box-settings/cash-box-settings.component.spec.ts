import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CashBoxSettingsComponent } from './cash-box-settings.component';

describe('CashBoxSettingsComponent', () => {
  let component: CashBoxSettingsComponent;
  let fixture: ComponentFixture<CashBoxSettingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CashBoxSettingsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CashBoxSettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
