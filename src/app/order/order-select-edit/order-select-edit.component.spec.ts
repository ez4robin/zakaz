import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderSelectEditComponent } from './order-select-edit.component';

describe('OrderSelectEditComponent', () => {
  let component: OrderSelectEditComponent;
  let fixture: ComponentFixture<OrderSelectEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrderSelectEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderSelectEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
