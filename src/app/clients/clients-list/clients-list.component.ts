import { Component, OnInit } from '@angular/core';

import {ColumnMode} from '@swimlane/ngx-datatable';
import {HttpClient} from '@angular/common/http';
@Component({
  selector: 'app-clients-list',
  templateUrl: './clients-list.component.html',
  styleUrls: ['./clients-list.component.scss']
})
export class ClientsListComponent implements OnInit {
  rows = [

  ];
  columns = [
    { prop: 'first_name', name: 'Имя'},
    { prop: 'phone', name: 'Телефон' },
  ];
  ColumnMode = ColumnMode;
  constructor(
    private http: HttpClient
  ) { }

  getClientsList() {
    this.http.get('api/clients').subscribe((data) => {
      this.rows = data['models'];
    });
  }

  ngOnInit() {
    this.getClientsList();
  }

}
