import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Client } from '../_models';

@Injectable()
export class ClientsService {
  constructor(private http: HttpClient) { }

  getAll(params = {}) {
    return this.http.get<Client[]>('/api/clients', params);
  }

  getById(id: number) {
    return this.http.get<Client>('/api/client/' + id);
  }

  create(user: any) {
    return this.http.post('/api/client', user);
  }

  update(user: Client) {
    return this.http.put('/api/client/' + user.id, user);
  }

  delete(id: number) {
    return this.http.delete('/api/client/' + id);
  }
}
