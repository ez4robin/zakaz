import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Schedule } from '../_models/schedule';

@Injectable()
export class UserScheduleService {
    constructor(private http: HttpClient) { }

    getAll(params = {}) {
        return this.http.get<Schedule[]>('/api/user-schedules', params);
    }

    create(model: any) {
        return this.http.post('/api/user-schedule', model);
    }

    update(model: any) {
        return this.http.post('/api/user-schedule-update/' + model.id, model);
    }

    delete(id: number) {
        return this.http.delete('/api/user-schedule/' + id);
    }
}
