/**
 * Created by Алексей on 09.02.2018.
 */
import {ActivatedRoute, ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {User} from '../_models';
import {Observable} from 'rxjs';
import {Injectable} from '@angular/core';
import {UserService} from '../_services';

@Injectable()
export class DriverNavigationResolver implements Resolve<User> {
    id: any;

    constructor(private userService: UserService, private router: ActivatedRoute) {

    }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<User> {
        console.log(route);
        return this.userService.getById(Number(route['_urlSegment']['segments'][2]['path']));
    }

}
