/**
 * Created by Алексей on 09.02.2018.
 */
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Car} from '../_models';
import {Observable} from 'rxjs';
import {Injectable} from '@angular/core';
import {UserCarsService} from '../_services';

@Injectable()
export class DriverCarsResolver implements Resolve<Car[]> {
    constructor(private service: UserCarsService) {

    }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Car[]> {
        return this.service.getAll({
            params: {
                id: Number(route['_urlSegment']['segments'][2]['path'])
            }
        });
    }

}
