/**
 * Created by Алексей on 09.02.2018.
 */
import {Resolve} from '@angular/router';
import {Observable} from 'rxjs';
import {RouterStateSnapshot} from '@angular/router';
import {ActivatedRouteSnapshot} from '@angular/router';
import {Injectable} from '@angular/core';

import {ClientsService} from '../_services';
import {Client} from '../_models';

@Injectable()
export class ClientsResolver implements Resolve<Client[]> {
    constructor(private clientsService: ClientsService) {

    }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Client[]> {
        return this.clientsService.getAll();
    }

}
