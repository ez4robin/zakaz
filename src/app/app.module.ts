﻿import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {CalendarModule} from 'angular-calendar';
import {AppComponent} from './app.component';
import {routing} from './app.routing';

import {AlertComponent} from './_directives';
import {AuthGuard} from './_guards';
import {JwtInterceptor} from './_helpers';
// import services
import {
    AlertService,
    AuthenticationService,
    CarService,
    ClientsService,
    OrderService,
    UserCarsService,
    UserContractService,
    UserProfileService,
    UserScheduleService,
    UserService
} from './_services';
import {HomeComponent} from './home';
import {LoginComponent} from './login';
import {RegisterComponent} from './register';
import {UsersResolver} from './_resolvers/user.resolve';
import {UserCarsResolver} from './_resolvers/user.cars';
import {DriversResolver} from './_resolvers/driver';
import {UserNavigationResolver} from './_resolvers/user.navigation';
import {NavigationComponent} from './nav/navigation.component';
import {UserComponent} from './user/user.component';
import {UserEditComponent} from './user/user-edit/user-edit.component';
import {UserEditResolver} from './_resolvers/user.edit';
import {UserContractResolver} from './_resolvers/user.contract';
import {LegalsResolver} from './_resolvers/legal';
import {UserNavigationComponent} from './user/user-navigation/user-navigation.component';
import {UserProfileComponent} from './user/user-profile/user-profile.component';
import {UserProfileResolver} from './_resolvers/user.profile';
import {UserScheduleResolver} from './_resolvers/user.schedule';
import {OrdersResolver} from './_resolvers/order.resolve';
import {OrderEditResolver} from './_resolvers/order.edit';
import {OrdersComponent} from './order/orders/orders.component';
import {OrderSelectEditResolver} from './_resolvers/order.select.edit';
import {OrderCreateComponent} from './order/order-create/order-create.component';
import {OrderEditComponent} from './order/order-edit/order-edit.component';
import {UserCarsComponent} from './user/user-cars/user-cars.component';
import {UserScheduleComponent} from './user/user-schedule/user-schedule.component';
import {StorageHelper} from './_helpers/storage.helper';
import {SafeHtmlPipe} from './_helpers/safe.html';
import {UserContractsComponent} from './user/user-contracts/user-contracts.component';
import {OrderSelectComponent} from './order/order-select/order-select.component';
import {OrderSelectEditComponent} from './order/order-select-edit/order-select-edit.component';
import {OrderSelectEditAdminComponent} from './order/order-select-edit/components/osea/order-select-edit-admin.component';
import {OrderSelectEditLegalComponent} from './order/order-select-edit/components/osel/order-select-edit-legal.component';
import {NotFoundComponent} from './not-found/not-found.component';
import {OrderAdminComponent} from './order/orders/order-admin/order-admin.component';
import {OrderDriverComponent} from './order/orders/order-driver/order-driver.component';
import {NgxDatatableModule} from '@swimlane/ngx-datatable';
import {OrderLegalComponent} from './order/orders/order-legal/order-legal.component';
import {CarsListResolver} from './_resolvers/car.list';
import {PartialsModule} from './partials/partials.module';
import {OrderClientEditComponent} from './order/order-client-edit/order-client-edit.component';
import {ClientsResolver} from './_resolvers/clients';
import {CashBoxSettingsComponent} from './cash-box-settings/cash-box-settings.component';

@NgModule({
    imports: [
        BrowserModule,
        FormsModule,
        HttpClientModule,
        ReactiveFormsModule,
        BrowserAnimationsModule,
        NgxDatatableModule,
        CalendarModule.forRoot(),
        PartialsModule,
        routing,
    ],
    declarations: [
        SafeHtmlPipe,
        AppComponent,
        AlertComponent,
        HomeComponent,
        LoginComponent,
        RegisterComponent,
        NavigationComponent,
        UserComponent,
        UserEditComponent,
        UserNavigationComponent,
        UserProfileComponent,
        OrdersComponent,
        OrderCreateComponent,
        OrderEditComponent,
        OrderDriverComponent,
        UserCarsComponent,
        UserScheduleComponent,
        UserContractsComponent,
        OrderSelectComponent,
        OrderSelectEditComponent,
        OrderSelectEditAdminComponent,
        OrderSelectEditLegalComponent,
        NotFoundComponent,
        OrderAdminComponent,
        OrderLegalComponent,
        OrderClientEditComponent,
        CashBoxSettingsComponent,
    ],
    providers: [
        StorageHelper,
        AuthGuard,
        AlertService,
        AuthenticationService,
        CarService,
        ClientsService,
        UserProfileService,
        UserContractService,
        UserService,
        UsersResolver,
        UserEditResolver,
        UserCarsResolver,
        UserProfileResolver,
        UserNavigationResolver,
        UserCarsService,
        OrderService,
        ClientsResolver,
        OrdersResolver,
        OrderEditResolver,
        OrderSelectEditResolver,
        UserScheduleService,
        UserScheduleResolver,
        DriversResolver,
        LegalsResolver,
        UserContractResolver,
        CarsListResolver,
        {
            provide: HTTP_INTERCEPTORS,
            useClass: JwtInterceptor,
            multi: true
        },
    ],
    bootstrap: [AppComponent]
})

export class AppModule {
}
